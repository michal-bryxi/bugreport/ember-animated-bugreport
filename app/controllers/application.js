import Controller from '@ember/controller';
import { toUp, toDown } from 'ember-animated/transitions/move-over';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ApplicationController extends Controller {
  rules({ oldItems, newItems }) {
    console.log(oldItems, newItems)
    if (oldItems[0] < newItems[0]) {
      return toDown;
    } else {
      return toUp;
    }
  }

  @tracked optionsSelected = 5;
  @tracked optionsLeft = 5;

  @action up() {
    this.optionsLeft = this.optionsLeft - 1;
    this.optionsSelected = this.optionsSelected + 1;
  }

  @action down() {
    this.optionsLeft = this.optionsLeft + 1;
    this.optionsSelected = this.optionsSelected - 1;
  }
}
